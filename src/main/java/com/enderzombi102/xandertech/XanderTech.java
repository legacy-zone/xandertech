package com.enderzombi102.xandertech;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Mod( modid = Const.ID )
public class XanderTech {
	public static final Logger LOGGER = LoggerFactory.getLogger( "XanderTech" );

	public XanderTech() {
		LOGGER.info( "Construction" );
	}

	public void onPreInit( FMLPreInitializationEvent evt ) {
		LOGGER.info( "PreInitialization" );
	}

	public void onInit( FMLInitializationEvent evt ) {
		LOGGER.info( "Initialization" );
	}

	public void onInit( FMLPostInitializationEvent evt ) {
		LOGGER.info( "PostInitialization" );
	}
}
