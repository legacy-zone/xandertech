@file:Suppress("UnstableApiUsage")
plugins {
    id( "xyz.wagyourtail.unimined" ) version "1.0.1"
	id( "com.github.johnrengelman.shadow" ) version "8.1.1"
}

val loaderVersion = "7.8.1.738"
val minecraftVersion = "1.5.2"

val shade by configurations.registering { }
val cleanLogs by tasks.registering {
	description = "Cleans the logs folder"

	doFirst { // delete all logs
		file( "run/logs" ).listFiles()?.forEach( File::delete )
	}
}

repositories {
    mavenCentral()
}

unimined {
	useGlobalCache = false

	minecraft {
		version( minecraftVersion )

//		mappings.mcp( "release", "7.51" )

		val configurer: xyz.wagyourtail.unimined.api.runs.RunConfig.() -> Unit = {
			workingDir = rootProject.file( "run" )
			runFirst += cleanLogs.get()
		}

		runs {
			config( "client", configurer )
			config( "server", configurer )
		}

		minecraftForge {
			loader( loaderVersion )
		}
	}
}

dependencies {
	implementation( "org.jetbrains:annotations:23.0.0" )
	"shade"( implementation( "org.slf4j:slf4j-api:2.0.7" )!! )
	"shade"( implementation( "org.slf4j:slf4j-jdk14:2.0.7" )!! )
}

tasks.withType<ProcessResources> {
	inputs.property( "version", project.version )
	inputs.property( "minecraft_version", minecraftVersion )
	inputs.property( "loader_version", loaderVersion )
	filteringCharset = "UTF-8"

	filesMatching( "mcmod.info" ) {
		expand(
			"version" to version,
			"minecraft_version" to minecraftVersion,
			"loader_version" to loaderVersion
		)
	}
}

tasks.withType<JavaCompile> {
	options.encoding = "UTF-8"
//	options.release.set( 8 )
}

tasks.withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
	configurations += project.configurations["shade"]
}

tasks.withType<Jar> {
	filesMatching( "LICENSE" ) {
		rename { "${it}_$archiveBaseName"}
	}
}
